package org.astm.collaboration.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Document(indexName = "pdfindex",type = "document")
public class Documents {

    @Id
    @Field(store = true, index = false, type = FieldType.Integer)
    public int id;

    @Field(type = FieldType.Nested)
    private Document document;

    @Override
    public String toString() {
        return "Documents{" +
            "id=" + id +
            ", document=" + document +
            '}';
    }

    /* @Field(type = FieldType.Text)
    public String content;

    @Field(type = FieldType.Text)
    public String metadata;*/

    /*public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }*/

    class Document {
//        @Field(type = FieldType.Text)
        private String filename;

//        @Field(type = FieldType.Text)
        private String file_location;

//        @Field(type = FieldType.Text)
        private String content;

        public String getFilename() {
            return filename;
        }

        public void setFilename(String filename) {
            this.filename = filename;
        }

        public String getFile_location() {
            return file_location;
        }

        public void setFile_location(String file_location) {
            this.file_location = file_location;
        }

        @Override
        public String toString() {
            return "Document{" +
                "filename='" + filename + '\'' +
                ", file_location='" + file_location + '\'' +
                ", content='" + content + '\'' +
                '}';
        }
    }
}
