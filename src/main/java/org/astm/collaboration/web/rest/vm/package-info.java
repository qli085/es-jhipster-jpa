/**
 * View Models used by Spring MVC REST controllers.
 */
package org.astm.collaboration.web.rest.vm;
