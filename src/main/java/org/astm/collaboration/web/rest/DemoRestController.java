package org.astm.collaboration.web.rest;

import org.astm.collaboration.domain.Documents;
import org.astm.collaboration.service.DocumentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/demo")
public class DemoRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DemoRestController.class);

    private final DocumentService documentService;

    public DemoRestController(DocumentService documentService) {
        this.documentService = documentService;
    }

    @GetMapping("/search")
    public List<Documents> search(@RequestParam("query") final String query) {
        System.out.println(documentService.findByFilename(query));
        LOGGER.info("search documents=\n", documentService.findByFilename(query));
        return documentService.findByFilename(query);
    }
}
