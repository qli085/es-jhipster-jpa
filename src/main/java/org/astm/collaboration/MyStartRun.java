package org.astm.collaboration;

import org.astm.collaboration.service.DocumentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

@Component
public class MyStartRun implements ApplicationRunner {

    private static final Logger LOGGER = LoggerFactory.getLogger(MyStartRun.class);

    private final DocumentService documentService;

    public MyStartRun(DocumentService documentService) {
        this.documentService = documentService;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        LOGGER.info("search", documentService.findAll(PageRequest.of(0, 5)));
    }
}
