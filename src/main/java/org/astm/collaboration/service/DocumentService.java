package org.astm.collaboration.service;

import org.astm.collaboration.domain.Documents;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface DocumentService {

    public void insert(Documents documents);

    public boolean delete(Documents documents);

    public boolean update(Documents documents);

    //查询全部
    public Iterable<Documents> findAll();
    //分页查询
    public Page<Documents> findAll(Pageable pageable);

    //根据文件名查询
    List<Documents> findByFilename(String condition);

    //根据文件名查询(含分页)
    Page<Documents> findByFilename(String condition, Pageable pageable);
}
