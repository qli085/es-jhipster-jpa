package org.astm.collaboration.service.impl;

import org.astm.collaboration.domain.Documents;
import org.astm.collaboration.repository.search.DocumentsRepository;
import org.astm.collaboration.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DocumentServiceImpl implements DocumentService {

    private final DocumentsRepository repository;

    public DocumentServiceImpl(DocumentsRepository repository) {
        this.repository = repository;
    }

    @Override
    public void insert(Documents documents) {

    }

    @Override
    public boolean delete(Documents documents) {
        return false;
    }

    @Override
    public boolean update(Documents documents) {
        return false;
    }

    @Override
    public Iterable<Documents> findAll() {
        return repository.findAll();
    }

    @Override
    public Page<Documents> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    @Override
    public List<Documents> findByFilename(String condition) {
        return repository.findByFilename(condition);
    }

    @Override
    public Page<Documents> findByFilename(String condition, Pageable pageable) {
        return repository.findByFilename(condition, pageable);
    }
}
