package org.astm.collaboration.repository.search;

import org.astm.collaboration.domain.Documents;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DocumentsRepository extends ElasticsearchRepository<Documents, String> {

    List<Documents> findByFilename(String condition);

    Page<Documents> findByFilename(String condition, Pageable pageable);
}
