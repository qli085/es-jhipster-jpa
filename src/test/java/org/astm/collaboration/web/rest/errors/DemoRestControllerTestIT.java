package org.astm.collaboration.web.rest.errors;

import org.astm.collaboration.EStestApp;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@AutoConfigureMockMvc
@SpringBootTest(classes = EStestApp.class)
public class DemoRestControllerTestIT {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void search() throws Exception {
        System.out.printf("111111111111111111111111111111111111111111111111111111111111111111111111111111111",
            mockMvc.perform(get("/demo/search?query=a")).andReturn().getResponse().getContentAsString());
        /*mockMvc.perform(get("/demo/search"))
            .andExpect(status().isConflict())
            .andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON));*/
//            .andExpect(jsonPath("$.message").value(ErrorConstants.ERR_CONCURRENCY_FAILURE));
    }
}
